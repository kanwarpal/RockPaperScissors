package com.company;
import java.util.Random;

public class Bot extends Player  {

@Override
public String makeMove(){
        int temp = randomNumberInRange();
        return super.getMovesAvailable().get(temp);
    }

// Referenced randomNumberInRange function from :
//http://www.technicalkeeda.com/java-tutorials/java-generate-random-numbers-in-a-specific-range
    public int randomNumberInRange() {
        int min = 0;
        int max = 2;
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
