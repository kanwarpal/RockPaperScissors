package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {
   private boolean gameMODE = true;
   private static Game game = null;
   private ArrayList<String> record = new ArrayList<>();

   private Game(){
     record.add("=== GAME HISTORY ===");
   }

   public static Game getGame(){
       if(game == null)
           game = new Game();
       return game;
   }

   String prompt(){
       String returnGameprompt;
       System.out.println("Welcome to Rock Paper Scissors!"+"\n\n");
       System.out.println("MAIN MENU"+"\n====="+"\n1. Type 'play' to play"+ "\n2. Type " +
               "'history' to view your game history"+"\nType 'quit' to stop playing\n\n");
       Scanner scanner = new Scanner(System.in);
       do{
           System.out.println("Only Pick: 'play'/'history'/'quit'");
           returnGameprompt = scanner.next();
           returnGameprompt = returnGameprompt.toLowerCase();
       }while (!returnGameprompt.equals("play") && !returnGameprompt.equals("history") && !returnGameprompt.equals("quit"));

       return returnGameprompt;
   }

   ArrayList<String> startplayHH(HumanPlayer player1, HumanPlayer player2){
        ArrayList<String> temp1 = new ArrayList<>();
        temp1.add(player1.makeMove());
        temp1.add(player2.makeMove());
        return temp1;
   }

   ArrayList<String> startplayHB(HumanPlayer player1, Bot player2){
        ArrayList<String> temp2 = new ArrayList<>();
        temp2.add(player1.makeMove());
        temp2.add(player2.makeMove());
        return temp2;
   }

   String results(ArrayList<String> playedMoves){
       String player1picked = playedMoves.get(0);
       String player2picked = playedMoves.get(1);
       String printable = "YOU picked=>"+player1picked+"\t&\t OPPONENT picked=>"+player2picked;

       if((player1picked.equals("rock") && player2picked.equals("scissors")) ||
               (player1picked.equals("scissors") && player2picked.equals("paper")) ||
               (player1picked.equals("paper") && player2picked.equals("rock"))){

           String addWin = "WIN: Player-"+player1picked+" Opponent-"+player2picked;
           record.add(addWin);
           return printable+"\n \t\t>>>>You WIN<<<<";
       }
       else if ((player1picked.equals("rock") && player2picked.equals("paper"))||
               (player1picked.equals("scissors") && player2picked.equals("rock"))||
               (player1picked.equals("paper") && player2picked.equals("scissors"))){

           String addLoss = "LOSS: Player-"+player1picked+" Opponent-"+player2picked;
           record.add(addLoss);
           return printable+"\n\t\t >>>>You LOSE<<<<";
       }
       else{
           String addTie = "TIE: Player-"+player1picked+" Opponent-"+player2picked;
           record.add(addTie);
           return printable+"\n \t\t>>>>Its a LOSE!!<<<<";}
    }

    public ArrayList<String> getRecord() {
        return record;
    }

    public void setRecord(ArrayList<String> record) {
        this.record = record;
    }

    public boolean isGameMODE() {
        return gameMODE;
    }

    public void setGameMODE(boolean gameMODE) {
        this.gameMODE = gameMODE;
    }


}
