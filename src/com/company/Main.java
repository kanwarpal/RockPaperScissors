package com.company;

public class Main {

    public static void main(String[] args) {

        Game game = Game.getGame();
        do {
            String gamePrompt = game.prompt();
            if (gamePrompt.equals("play")) {
                HumanPlayer player1 = new HumanPlayer();
                if (player1.prompt().equals("human")) {
                    HumanPlayer player2 = new HumanPlayer();
                    System.out.println(game.results(game.startplayHH(player1, player2)));
                } else {
                    Bot player2 = new Bot();
                    System.out.println(game.results(game.startplayHB(player1, player2)));
                }
            } else if (gamePrompt.equals("history")) {
                for(int k =0;k<game.getRecord().size();k++){
                    System.out.println(game.getRecord().get(k));
                }
                System.out.println();
            } else{
                System.out.println("Thank you!");
                game.setGameMODE(false);
            }
        }while (game.isGameMODE());
    }

}