package com.company;

import java.util.Scanner;

public class HumanPlayer extends Player {

    String prompt(){
        String returnHPprompt;
        System.out.println("whom do you want to play against:\n\tA Human\n\tor\n\tA Bot\n\n");
        Scanner opponent = new Scanner(System.in);
        do {
            System.out.println("Pick one('human'/'bot')");
            returnHPprompt = opponent.next();
            returnHPprompt = returnHPprompt.toLowerCase();
        }while (!returnHPprompt.equals("human") && !returnHPprompt.equals("bot"));
        return returnHPprompt;
    }

    @Override
    public String makeMove(){
        int index = 0;
        String userInput;
        Scanner userMademove = new Scanner(System.in);
        do {
            System.out.println("ONLY pick: rock/scissors/paper");
            userInput = userMademove.next();
            userInput = userInput.toLowerCase();
        }while (!userInput.equals("rock") && !userInput.equals("scissors") && !userInput.equals("paper"));
        for(int i=0;i<=super.getMovesAvailable().size();i++){
            if(userInput.equals(super.getMovesAvailable().get(i)))
            {
                index = i;
                break;
            }
        }
        return super.getMovesAvailable().get(index);
    }


}
