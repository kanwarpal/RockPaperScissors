package test.java.com.company;

import com.company.Bot;
import org.junit.Test;
import static org.junit.Assert.*;
public class BotTest {

    Bot botObject = new Bot();
    String actualReturnedString = botObject.makeMove();
    //int actual  = botObject.randomNumberInRange(1,3);
@Test
public void testmakeMoveeCase1(){

    String expectedOption1 = "rock";
    assertEquals(expectedOption1,actualReturnedString);

}
@Test
public void testmakeMoveeCase2(){

    String expectedOption2 = "paper";
    assertEquals(expectedOption2,actualReturnedString);
}
@Test
public void testmakeMoveeCase3(){
    String expectedOption3 = "scissors";
    assertEquals(expectedOption3,actualReturnedString);
}


}
